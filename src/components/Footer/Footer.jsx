import styled from "styled-components"

const FooterSt=styled.footer`
display:flex;
margin-top:3rem;
background-color: black;
color:white;
width:100%;
height:4rem;
a{
    color: white;
margin-left: auto;
margin-right: auto;
}
p{
    padding-right:3rem;
}
`;


const Footer = ()=>{
    return(
<FooterSt> 
<a href="/">'Політика конфіденційності'</a>
<p>Copyright © 2011 - 2023 Diplomus</p>
</FooterSt>
)}
export default Footer