
import {useEffect, useState} from "react";
import PropTypes from 'prop-types'
import {sendRequest} from "../../helpers/sendRequest";
import CardList from './Components/CardList';
import ModalImage from '../Modal/ModalImage/ModalImage';
import {ReactComponent as Favorite} from '../../icons/favorite.svg';



const ShowCardList = ({cart, favorite, onClickIcon, onClicAddCart}) => {
	const [productArray, setProductArray] = useState([])
	useEffect(() => {
		sendRequest(window.location.origin +'/products.json')
			.then((date) => {	
				setProductArray(date);
			})
		}, [])
	const [isModalImage,setIsModalImage] = useState(false)
	const handleModalImage = () => setIsModalImage(!isModalImage)

	const [currentPost, setCurrentPost] = useState({})
	const handleCurrentPost = (cardPost) => setCurrentPost(cardPost)
	

	return (
		<section className="pt-5 p-2 text-center">
			<h2 className='text-center p-2'>Керамические обогреватели</h2>
			<CardList favorite={favorite} date={productArray} handleModal={handleModalImage} handleCurrentPost={handleCurrentPost}
				onClickIcon={onClickIcon}><Favorite/>
			</CardList>
			<ModalImage
                    isOpen={isModalImage}
                    handleClose={handleModalImage}
					products={currentPost}
					cart={cart}
					handleOk={() => {
						onClicAddCart(currentPost)
					}}
           />
		</section>
	)
}
ShowCardList.defaultProps = {
    onClickIcon: () => {},
	onClicAddCart: () => {}
  }
ShowCardList.propTypes = {
	cart: PropTypes.array,
	favorite: PropTypes.array,
    onClickIcon: PropTypes.func,
	onClicAddCart: PropTypes.func
}
export default ShowCardList

