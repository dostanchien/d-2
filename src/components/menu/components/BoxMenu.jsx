import PropTypes from 'prop-types'
import cx from "classnames"
import './Favorite.scss';


const BoxMenu = ({children, title, count, onClick}) =>{
    return(
                    <div className="header__actions" onClick={onClick}>
                        <div className="header__favorites-list">
                            {title}      
                            <span className={cx("icon-favorite",(count===0) ? 'text-white' : 'text-red')} >
                                <span className="count">{count}</span>
                            {children}                        
                            </span>
                        </div>
                    </div>
    )
}
BoxMenu.defaultProps = {
    onClick: () => {}
  }
BoxMenu.propTypes = {
    children: PropTypes.any,
    title: PropTypes.string,
    count: PropTypes.number,
    onClick: PropTypes.func
}
export default BoxMenu



                  