import React from 'react';
import { useLocalStorage} from "./helpers/useLocalStorage";
import NavState from './components/menu/context/navState';
import MainMenu from './components/menu/components/MainMenu';
import ShowCardList from './components/Card/ShowCardList';
import Footer from './components/Footer/Footer';
function App() {
	
	const [carts, setCart] = useLocalStorage("CookiCart", "");
	const [favorites, setFavorite] = useLocalStorage("CookiFavorite", "");

	const handleFavorites = (products) => {
		const isAdded = favorites.some((favorite)=> favorite.article === products.article)
		if(isAdded){
			setFavorite(favorites.filter((favorite)=> favorite.article !== products.article));
			return
		}
		setFavorite([...favorites,products])
	}

	const handleCart = (products) => {
		const isAdded = carts.some((cart)=> cart.article === products.article)
		isAdded ?
				setCart(carts.filter((cart)=> cart.article !== products.article))
				:
				setCart([...carts,products]);
	}

  return (
  <>
     <NavState>
      <MainMenu  countFavorite={favorites.length} countCart={carts.length}/>
    </NavState>
	<ShowCardList cart={carts} favorite={favorites} onClickIcon={handleFavorites} onClicAddCart={handleCart}/>
	<Footer/>
   </>
  );
}
export default App;

