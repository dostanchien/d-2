import { useState, useEffect } from "react";
export const useLocalStorage = (key,defaultValue) => {
      const [state, setState] = useState([]);
      useState(() => {
            const state = JSON.parse(localStorage.getItem(key));
            if (state) {
                  setState(state);
            }

}, []);
console.log(state)
      useEffect(()=>{
                      localStorage.setItem(key, JSON.stringify(state))
                    }, [key,state])
      return [state, setState]
}


