/*Навбар
Остался последний компонент, навбар. О функции useOnClickOutside будет идти речь ниже, а пока всё оставим как есть.

//import {ReactComponent as Favorite} from '../../../icons/favorite.svg?react';
src/components/MainMenu.js */
import React, { useRef, useContext } from 'react';
import PropTypes from 'prop-types'
import styled from 'styled-components';
import useOnClickOutside from '../hooks/onClickOutside';
import { MenuContext } from '../context/navState';
import HamburgerButton from './HamburgerButton';
import { SideMenu } from './SideMenu';
import './Favorite.scss';
import {ReactComponent as Favorite} from '../../../icons/favorite.svg?react';
import {ReactComponent as Cart} from '../../../icons/cart.svg';
import BoxMenu from './BoxMenu';

const Navbar = styled.div`
  display: flex;
  position: fixed;
  left: 0;
  right: 0;
  box-sizing: border-box;
  outline: currentcolor none medium;
  max-width: 100%;
  margin: 0px;
  align-items: center;
  background: #082bff none repeat scroll 0% 0%;
  color: rgb(248, 248, 248);
  min-width: 0px;
  min-height: 0px;
  flex-direction: row;
  justify-content: flex-start;
  padding: 6px 60px;
  box-shadow: rgba(0, 0, 0, 0.2) 0px 8px 16px;
  z-index: 500;
`;

const MainMenu = ({countFavorite,countCart,onClick}) => {
  const node = useRef();
  const { isMenuOpen, toggleMenuMode } = useContext(MenuContext);
  useOnClickOutside(node, () => {
    // Only if menu is open
    if (isMenuOpen) {
      toggleMenuMode();
    }
  });
  return (
    <header ref={node}>
      <Navbar>
        <HamburgerButton />
        <h1>Website</h1>
        <div className="wraper-box-menu">
        <BoxMenu 
            title={"Cписок избранных"}
            count={countFavorite}
            >
            <Favorite/> 
          </BoxMenu>
          <BoxMenu      
            title={"Корзина товаров"}
            count={countCart} 
            //переходим на страницу корзины onClick={} onClick={openCart}
          >
            <Cart/>  
          </BoxMenu>
        </div>
      </Navbar>
      <SideMenu />
    </header>
  );
};
MainMenu.defaultProps = {
  click: () => {}
}
MainMenu.propTypes = {
  countCart: PropTypes.number,
  countFavorite: PropTypes.number,
  onClick: PropTypes.func
}
export default MainMenu;